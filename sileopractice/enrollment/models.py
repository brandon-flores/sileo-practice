# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Adviser(models.Model):
    adviser_name = models.CharField(max_length=100)

    def __str__(self):
        return self.adviser_name


class Student(models.Model):
    # pass
    adviser = models.ForeignKey(Adviser)
    student_id = models.CharField(max_length=10)
    student_name = models.CharField(max_length=100)

    def __str__(self):
        return self.student_id


class Course(models.Model):
    # pass
    course_code = models.CharField(max_length=10)
    course_title = models.CharField(max_length=100)
    students_enrolled = models.ManyToManyField(Student)

    def __str__(self):
        return self.course_code


'''
enrollment

| student |

'''
