from sileo.forms import ModelForm
from enrollment.models import Course


class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = ['course_code', 'course_title', 'students_enrolled']