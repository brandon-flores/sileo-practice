from sileo.forms import ModelForm
from enrollment.models import Student


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ('student_id', 'student_name', 'adviser',)
