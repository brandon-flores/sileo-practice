# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class EnrollmentConfig(AppConfig):
    name = 'enrollment'

    def ready(self):
        from enrollment import api_sileo
