# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Student, Course, Adviser

# class ChoiceInLine(admin.TabularInline):
#     model = Course
#     extra = 2

class CourseAdmin(admin.ModelAdmin):
    fields = ['students_enrolled', 'course_code', 'course_title']
    list_display = ['get_students']
    # fields = ['student_id', 'student_name']
    # fieldsets = [
    #     ('Student Information', {'fields': ['student_id', 'student_name']})
    # ]
    # inlines = [ChoiceInLine]
    def get_students(self, obj):
        # print obj.students_enrolled.all()
        for s in obj.students_enrolled.all():
            print s.student_id
        # for s.student_id for s in obj.students_enrolled.all()
        return "\n".join([s.student_id for s in obj.students_enrolled.all()])

# Register your models here.
admin.site.register(Student)
admin.site.register(Course)
admin.site.register(Adviser)