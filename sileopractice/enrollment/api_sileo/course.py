from sileo.resource import Resource
from sileo.registration import register
from sileo.fields import ResourceMethodField

from enrollment.forms.course_form import CourseForm
from enrollment.models import Course


class CourseResource(Resource):
    query_set = Course.objects.all()
    fields = ['course_code', 'course_title', ResourceMethodField(
        'students_enrolled', method_name='get_students')]
    allowed_methods = ['filter']
    filter_fields = ['course_code__icontains']
    form_class = CourseForm


    def get_students(self, prop, obj, request):
        return [s.student_name for s in obj.students_enrolled.all()]

register(namespace="enrollment", name="course", resource=CourseResource)
