from sileo.resource import Resource
from sileo.registration import register
from sileo.fields import ResourceMethodField, ResourceModel, ResourceTypeConvert
from enrollment.models import Student
from enrollment.forms.student_form import StudentForm
# , Adviser


class StudentResource(Resource):
    query_set = Student.objects.all()
    fields = ['student_id', 'student_name']
    allowed_methods = ['get_pk', 'filter', 'create', 'update', 'delete']
    filter_fields = ['student_name__icontains']
    form_class = StudentForm

# class AdviserResource(Resource):
#     query_set = Adviser.objects.all()
#     fields = ['adviser_name']


class CourseListResource(Resource):
    query_set = Student.objects.all()
    # ResourceModel('adviser', resource=AdviserResource))
    fields = ['student_name', ResourceTypeConvert('adviser', str),
              ResourceMethodField('course_list', method_name='get_courses')
              ]
    allowed_methods = ['filter']
    filter_fields = ['student_name__icontains']
    # filter_fields = ['course__code']

    def get_courses(self, prop, obj, request):
        return [s.course_code for s in obj.course_set.all()]


register(namespace='enrollment', name='student', resource=StudentResource)
register(namespace='enrollment', name='course_list',
         resource=CourseListResource)
