from sileo.resource import Resource
from sileo.registration import register
from sileo.fields import ResourceMethodField

from enrollment.models import Adviser


class AdviserReource(Resource):
    query_set = Adviser.objects.all()
    fields = ['adviser_name']
    allowed_methods = ['filter']
    filter_fields = ['adviser_name__icontains']


class AdviseeListResource(Resource):
    query_set = Adviser.objects.all()
    fields = ['adviser_name', ResourceMethodField(
        'advisee_list', method_name='get_advisees')]
    allowed_methods = ['filter']
    filter_fields = ['adviser_name__icontains']

    def get_advisees(self, prop, obj, request):
        return [[s.student_id, s.student_name] for s in obj.student_set.all()]


register(namespace='enrollment', name='adviser', resource=AdviserReource)
register(namespace='enrollment', name='advisee_list',
         resource=AdviseeListResource)
